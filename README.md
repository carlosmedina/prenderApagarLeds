Project plan:

Requirements
Design and architecture
Programming
Tests
Documentation

WorkFlow
In the market there are several types of Workflow tools, which will be used is: Application Workflow. 
This is limited to your particular area and allows communication with external applications synchronously 
(waiting for the answer before proceeding) and / or asynchronous (only leaves a "message" and retrieves the answer later).

Good practices:

The good practices that will be used are the Agil methodology, the use of sprints will also be done through the tools provided 
in git and it will be used to establish user stories, which will help us a lot in the documentation of the development of the project.

Project revisions will be made using the tools provided by git.

Ernesto Lerma
Carlos Medina