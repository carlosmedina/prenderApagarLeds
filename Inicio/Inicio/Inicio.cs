﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Inicio.Controllers;

namespace Inicio
{
    public partial class Inicio : Form
    {
        public Inicio()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ControladorLogin controlador = new ControladorLogin();
            var acceso = controlador.loginExisteUsuario(textBox1.Text, textBox2.Text);
            ListadeUsuarios listaform = new ListadeUsuarios();

            if (acceso)
            {
                MessageBox.Show("Accesso aprobado");
                this.Hide();
                listaform.IdUsuarioLog = controlador.IdUsuarioLog(textBox1.Text, textBox2.Text);
                var usuario = controlador.UsuarioInfo(listaform.IdUsuarioLog);
                if (usuario.rol == 1)
                {
                    listaform.rol = Convert.ToInt32(controlador.IdUsuarioLog(textBox1.Text, textBox2.Text));
                    listaform.Show();
                }

            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var ventanaUsuario = new CrearUsuario();
            ventanaUsuario.Show();
        }
    }
}
