﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Inicio.Controllers;

namespace Inicio
{
    public partial class ListadeUsuarios : Form
    {
        public int IdUsuarioLog { get; set; }
        public int IdUsuario { get; set; }
        public int rol { get; set; }

        public ListadeUsuarios()
        {
            InitializeComponent();
        }

        private void ListadeUsuarios_Load(object sender, EventArgs e)
        {
            rol = this.rol;
            IdUsuarioLog = this.IdUsuarioLog;           
            dataGridView2.Visible = false;
            ControladorLogin controlador = new ControladorLogin();
            var lista = controlador.ListaUsuarios();

            dataGridView1.DataSource = lista;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[e.RowIndex];
            if (row.Index != null)
            {
                textBox1.Text = (row.Cells[0].Value).ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Seleccione usuario a eliminar");
            }
            else
            {
                CrearUsuario crearUsuario = new CrearUsuario();
                crearUsuario.IdUsuarioLog = IdUsuarioLog;
                crearUsuario.rol = rol;
                crearUsuario.IdUsuario = Convert.ToInt32(textBox1.Text);
                crearUsuario.evento = "Eliminar";
                this.Close();
                crearUsuario.Show();
            }
        }

        private void dataGridView1_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[e.RowIndex];
            if (row.Index != null)
            {
                var idUsuario = Convert.ToInt32(row.Cells[0].Value);
                CrearUsuario crearUsuario = new CrearUsuario();
                crearUsuario.IdUsuario = idUsuario;
                crearUsuario.rol = rol;
                crearUsuario.IdUsuarioLog = IdUsuarioLog;
                crearUsuario.evento = "Modificar";
                this.Close();
                crearUsuario.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Text == "About")
            {
                pictureBox1.Visible = true;
                button2.Text = "Close About";
            }
            else
            {
                button2.Text = "About";
                pictureBox1.Visible = false;
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView1.Visible = false;
            dataGridView2.Visible = true;
            label1.Visible = false;

            ControladorLogin controlador = new ControladorLogin();
            var lista = controlador.ListaAccionUsuarios();

            dataGridView2.DataSource = lista;
        }
    }
}
