﻿using System;
using Inicio;
using Inicio.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DALedsPruebasUnitarias
{
    [TestClass]
    public class PruebasAct2
    {
        [TestMethod]
        public void listaUsuarios()
        {
            ControladorLogin controlador = new ControladorLogin();

            //Act
            var usuarioPrueba = controlador.ListaAccionUsuarios();

            //Asset
            Assert.IsNotNull(usuarioPrueba);
        }

        [TestMethod]
        public void usuarioInfo()
        {
            ControladorLogin controlador = new ControladorLogin();
            var idUsuario = 2;
            //Act
            var usuarioPrueba = controlador.UsuarioInfo(idUsuario);

            //Asset
            Assert.IsNotNull(usuarioPrueba);
        }
    }
}