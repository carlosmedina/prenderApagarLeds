﻿using System;
using System.Text;
using System.Collections.Generic;
using Inicio.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DALedsPruebasUnitarias
{
    /// <summary>
    /// Descripción resumida de UnitTest1
    /// </summary>
    [TestClass]
    public class PruebasAct1
    {
        [TestMethod]
        public void loginUsuario()
        {
            var correo = "cmc";
            var contrasena = "1234";
            ControladorLogin controlador = new ControladorLogin();

            //Act
            var usuarioPrueba = controlador.loginExisteUsuario(correo, contrasena);

            //Asset
            Assert.IsTrue(usuarioPrueba);
        }
    }
}
