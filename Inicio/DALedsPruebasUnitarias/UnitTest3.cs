﻿using Inicio.Controllers;
using Inicio.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DALedsPruebasUnitarias
{
    [TestClass]
    public class PruebasAct3
    {
        [TestMethod]
        public void pruebaCrearUsuarios()
        {
            Usuario usuario = new Usuario();
            usuario.contrasena = "123";
            usuario.correo = "cmc";
            usuario.nombreUsuario = "hugo";
            usuario.rol = 1;

            ControladorLogin controlador = new ControladorLogin();

            //Act
            var usuarioPrueba = controlador.Guardar(usuario);

            //Asset
            Assert.IsNotNull(usuarioPrueba);
        }
    }
}
